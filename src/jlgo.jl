module JlGo
include("gotypes.jl")
include("zobrist.jl")
include("goboard.jl")
include("scoring.jl")
include("agent.jl")
include("mcts.jl")
include("main.jl")
end # module
