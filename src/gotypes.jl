module GoTypes
using Test
export neighbors, other, Point
# Player

function other(player)
    if player == :black
        return :white
    elseif player == :white
        return :black
    else
        throw(ErrorException("player must be :black or :white"))
    end
end

# Point

const Point = NamedTuple{(:row,:col),Tuple{Int,Int}}

function neighbors(point::Point)
    [(row=point.row-1,col=point.col),
     (row=point.row+1,col=point.col),
     (row=point.row,col=point.col-1),
     (row=point.row,col=point.col+1)]
end

@testset "neighbor tests" begin
    @test neighbors((row=2,col=3)) == [(row=1,col=3),
                                       (row=3,col=3),
                                       (row=2,col=2),
                                       (row=2,col=4)]
    @test neighbors((row=3,col=2)) == [(row=2,col=2),
                                       (row=4,col=2),
                                       (row=3,col=1),
                                       (row=3,col=3)]
end
@testset "player tests" begin
    @test other(:black) == :white
    @test_throws ErrorException other(:green)
end
end
