module GoBoard_Slow
using FunctionalCollections, Test
using ..GoTypes

export new_game
export Move, MovePlay, MovePass, MoveResign
export is_on_grid, get_player, get_string
export apply_move, is_valid_move, is_over
# Move
abstract type Move end
struct MovePlay <: Move
    point::Point
end

struct MovePass <: Move
end

struct MoveResign <: Move
end

# GoString

make_gostring(color) = PersistentHashMap(:color => color,
                                         :liberties => PersistentSet{Point}(),
                                         :stones => PersistentSet{Point}())

make_gostring(color,stones,liberties) =
    PersistentHashMap(:color => color,
                      :stones => stones,
                      :liberties => liberties)
                                                          

add_liberty(string, point) = assoc(string,:liberties,push(string[:liberties],point))

remove_liberty(string, point) = assoc(string,:liberties,
                                      disj(string[:liberties],point))

num_liberties(string) = length(string[:liberties])

function merged_with(string,other)
    @assert string[:color] == other[:color]
    combined_stones = union(string[:stones],other[:stones])
    make_gostring(string[:color],combined_stones,
                  setdiff(union(string[:liberties],other[:liberties]),combined_stones))
end

# GoBoard

is_on_grid(board,point) = (1 <= point.row <= board[:num_rows]) &&
    (1 <= point.col <= board[:num_cols])

function get_player(board,point)
    if !haskey(board[:grid],point)
        return nothing
    else
        return board[:grid][point][:color]
    end
end

function get_string(board,point)
    if !haskey(board[:grid],point)
        return nothing
    else
        return board[:grid][point]
    end
end

function place_stone(board, player, point)
    function remove_string(string)
        for point in string[:stones]
            for neighbor in neighbors(point)
                neighbor_string = if !haskey(grid,neighbor)
                    nothing
                else
                    grid[neighbor]
                end
                if neighbor_string == nothing
                    continue
                elseif neighbor_string != string
                    neighbor_string = add_liberty(neighbor_string, point)
                    for neighbor_point in neighbor_string[:stones]
                        grid = assoc(grid, neighbor_point, neighbor_string)
                    end
                end
            end
            grid = dissoc(grid, point)
        end
    end
    function get_adjacent(player)
        PersistentSet(filter(x -> x != nothing && x[:color] == player,
                             [get_string(board,n) for n in neighbors(point)]))
    end

    @assert is_on_grid(board,point)
    @assert get_string(board,point) == nothing

    liberties = PersistentSet(filter(x -> is_on_grid(board,x) && get_string(board,x) == nothing,
                       [n for n in neighbors(point)]))

    new_string = reduce(merged_with,
                        get_adjacent(player);
                        init=make_gostring(player, conj(PersistentSet(),point), liberties))

    grid = reduce((g, p) -> assoc(g, p, new_string),
                  new_string[:stones];
                  init=board[:grid])

    for s in get_adjacent(other(player))
        if length(disj(s[:liberties],point)) > 0
            new_opp_string = make_gostring(other(player), s[:stones], disj(s[:liberties],point))
            grid = reduce((g,p) -> assoc(g,p,new_opp_string), s[:stones]; init=grid)
        end
    end
    for s in get_adjacent(other(player))
        if length(disj(s[:liberties],point)) == 0
            remove_string(s)
        end
    end
    assoc(board, :grid, grid)
end

function show_board(board)
    for i in board[:num_rows]:-1:1
        for j in 1:board[:num_cols]
            if get_player(board,(row=i,col=j)) == nothing
                print("_")
            elseif get_player(board,(row=i,col=j)) == :black
                print("b")
            else
                print("w")
            end
        end
        println()
    end
    println()
end

# GameState

function new_game(board_rows, board_cols)
    return PersistentHashMap(:next_player => :black,
                      :board => PersistentHashMap(:num_rows => board_rows,
                                                  :num_cols => board_cols,
                                                  :grid => PersistentHashMap{Point,PersistentHashMap}()),
                      :previous_state => nothing,
                      :last_move => nothing)
end

new_game(size) = new_game(size, size)

function apply_move(state, move::MovePlay)
    PersistentHashMap(:next_player => other(state[:next_player]),
                      :previous_state => state,
                      :last_move => move,
                      :board => place_stone(state[:board], state[:next_player], move.point))
end

function apply_move(state, move::Move)
    PersistentHashMap(:next_player => other(state[:next_player]),
                      :previous_state => state,
                      :last_move => move,
                      :board => state[:board])
end

function is_over(state)
    _is_over(::Nothing, ::Any) = false
    _is_over(::MoveResign, ::Move) = true
    _is_over(::MoveResign, ::Nothing) = true
    _is_over(::MovePlay, ::Nothing) = false
    _is_over(::MovePass, ::Nothing) = false
    _is_over(::MovePass, ::MovePass) = true
    _is_over(::Move, ::Move) = false
    second_last_move = if state[:previous_state] == nothing
        nothing
    else
        state[:previous_state][:last_move]
    end
    _is_over(state[:last_move], second_last_move)
end

is_move_self_capture(state, player, move::Move) = false

function is_move_self_capture(state, player, move::MovePlay)
    num_liberties(get_string(place_stone(state[:board], player, move.point), move.point)) == 0
end

is_move_ko(state, player, move::Move) = false

function is_move_ko(state, player, move::MovePlay)
    situation(state) = (state[:next_player], state[:board])
    board = place_stone(state[:board], player, move.point)
    next_situation = (other(player), board)
    past_state = state[:previous_state]
    while past_state != nothing
        if situation(past_state) == next_situation
            return true
        else
            past_state = past_state[:previous_state]
        end
    end
    return false
end

function is_valid_move(state, move)
    _is_valid_move(::Move) = true
    _is_valid_move(move::MovePlay) = get_string(state[:board], move.point) == nothing &&
        !is_move_self_capture(state, state[:next_player], move) &&
        !is_move_ko(state, state[:next_player], move)
    if is_over(state)
        return false
    else
        _is_valid_move(move)
    end
end

@testset "GoString tests" begin
    @test make_gostring(:black) ==
        PersistentHashMap(:color => :black,
                          :liberties => PersistentSet(),
                          :stones => PersistentSet())
    s = make_gostring(:black)
    @test add_liberty(s,(row=1,col=1))[:liberties] == push(PersistentSet(),(row=1,col=1))
    s = add_liberty(s, (row=1,col=1))
    @test add_liberty(s,(row=2,col=2))[:liberties] ==
        push(push(PersistentSet(),(row=1,col=1)),(row=2,col=2))
    s = add_liberty(s, (row=2,col=2))
    @test remove_liberty(s,(row=1,col=1))[:liberties] ==
        push(PersistentSet(),(row=2,col=2))
    @test remove_liberty(s,(row=2,col=2))[:liberties] ==
        push(PersistentSet(),(row=1,col=1))
    t = add_liberty(make_gostring(:black),(row=3,col=4))
    u = add_liberty(make_gostring(:white),(row=3,col=4))
    v = add_liberty(make_gostring(:white),(row=3,col=4))
    @test_throws AssertionError merged_with(s,u)
    @test merged_with(s,t)[:liberties] ==
        push(push(push(PersistentSet(),(row=1,col=1)),(row=2,col=2)),(row=3,col=4))
    @test s != t
    @test u == v
end
@testset "GoBoard tests" begin
    b = PersistentHashMap(:num_rows => 19,
                          :num_cols => 19,
                          :grid => PersistentHashMap{Point,PersistentHashMap}())
    @test is_on_grid(b,(row=0,col=1)) == false
    @test is_on_grid(b,(row=1,col=1)) == true
    @test is_on_grid(b,(row=1,col=0)) == false
    @test is_on_grid(b,(row=1,col=20)) == false
    @test is_on_grid(b,(row=20,col=1)) == false
    @test_throws AssertionError place_stone(b,:black,(row=-1,col=10))
    @test get_player(b,(row=1,col=1)) == nothing
    @test get_string(b,(row=1,col=1)) == nothing
    @test place_stone(b,:white,(row=1,col=1))[:grid][(row=1,col=1)][:color]==:white
    b2 = place_stone(b,:white,(row=1,col=1))
    b2 = place_stone(b2,:black,(row=1,col=2))
    b2 = place_stone(b2,:black,(row=2,col=1))
    @test haskey(b2[:grid],(row=1,col=1)) == false
    @test in((row=1,col=1), get_string(b2,(row=2,col=1))[:liberties]) == true
    b2 = place_stone(b,:white,(row=1,col=1))
    b2 = place_stone(b2,:white,(row=2,col=1))
    @test b2[:grid][(row=1,col=1)] == b2[:grid][(row=2,col=1)]
    @test new_game(19,19)[:next_player] == :black
end
@testset "GoState tests" begin
    state = new_game(19,19)
    @test apply_move(state, MovePlay((row=1,col=1)))[:next_player] == :white
    @test apply_move(state, MovePass())[:next_player] == :white
    @test is_over(apply_move(state,MoveResign())) == true
    @test is_over(apply_move(state, MovePlay((row=1,col=1)))) == false
    @test is_over(state) == false
    @test is_over(apply_move(apply_move(state,MovePass()), MovePass())) == true
    @test is_over(apply_move(apply_move(state,MovePass()), MoveResign())) == true
    @test is_move_self_capture(state, :white, MovePass()) == false
    next_state = apply_move(state, MovePlay((row=1,col=1))) # b
    next_state = apply_move(next_state, MovePlay((row=2,col=1))) # w
    next_state = apply_move(next_state, MovePlay((row=19,col=1))) # b
    next_state = apply_move(next_state, MovePlay((row=2, col=2))) # w
    next_state = apply_move(next_state, MovePlay((row=19,col=2))) # b
    next_state_sc = apply_move(next_state, MovePlay((row=1, col=3))) # w
    @test is_move_self_capture(next_state_sc, :black, MovePlay((row=1,col=2))) == true
    @test is_move_self_capture(next_state_sc, :black, MovePlay((row=19,col=3))) == false
    @test is_move_ko(state, :black, MovePlay((row=1,col=1))) == false
    next_state = apply_move(state, MovePlay((row=1,col=2))) # b
    next_state = apply_move(next_state, MovePlay((row=2,col=2))) # w
    next_state = apply_move(next_state, MovePlay((row=2,col=1))) # b
    next_state = apply_move(next_state, MovePlay((row=3,col=1))) # w
    next_state = apply_move(next_state, MovePlay((row=2,col=3))) # b
    next_state = apply_move(next_state, MovePlay((row=3,col=3))) # w
    next_state = apply_move(next_state, MovePass()) # b
    next_state = apply_move(next_state, MovePlay((row=4,col=2))) # w
    next_state_ko = apply_move(next_state, MovePlay((row=3,col=2))) # b
    @test is_move_ko(next_state_ko, :white, MovePlay((row=2,col=2))) == true
    @test is_valid_move(apply_move(state, MoveResign()), MovePlay((row=1,col=1))) == false
    @test is_valid_move(state, MovePass()) == true
    @test is_valid_move(apply_move(state, MovePlay((row=1,col=1))), MovePlay((row=1,col=1))) == false
    @test is_valid_move(next_state_sc, MovePlay((row=1,col=2))) == false
    @test is_valid_move(next_state_ko, MovePlay((row=2,col=2))) == false
    @test is_valid_move(state, MovePlay((row=1,col=1))) == true
end

end # module
