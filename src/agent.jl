module agent
include("agent_helpers.jl")

using Random
using ..goboard, .helpers, ..scoring

export Agent, AlphaBetaAgent, FastRandomAgent, select_move

abstract type Agent end

function select_move(agent::Agent, state)
    throw(ErrorException("Not implemented"))
end

mutable struct FastRandomAgent <: Agent
    dim::Tuple{Int,Int}
    point_cache::Array
end

function FastRandomAgent()
    FastRandomAgent((0,0),[])
end

function update_cache(agent, dim)
    agent.dim = dim
    rows, cols = dim
    agent.point_cache = []
    for r in 1:rows
        for c in 1:cols
            push!(agent.point_cache, (row=r,col=c))
        end
    end
end

function select_move(agent::FastRandomAgent, state)
    dim = (state.board.num_rows, state.board.num_cols)
    if dim != agent.dim
        update_cache(agent, dim)
    end

    idx = collect(1:length(agent.point_cache))
    shuffle!(idx)
    for i in idx
        p = agent.point_cache[i]
        if is_valid_move(state, MovePlay(p)) && !is_eye(state.board, p, state.next_player)
            return MovePlay(p)
        end
    end
    MovePass()
end

struct AlphaBetaAgent <: Agent
    max_depth::Int
    eval_fn
end

function select_move(agent::AlphaBetaAgent, game_state)
    if winner(compute_game_result(game_state)) == game_state.next_player && game_state.last_move == MovePass()
        MovePass()
    else
        alpha_beta_search(game_state,
                          agent.max_depth,
                          agent.eval_fn,
                          -(game_state.board.num_rows * game_state.board.num_cols),
                          -(game_state.board.num_rows * game_state.board.num_cols))[2]
    end
end

function alpha_beta_search(game_state, max_depth, eval_fn, alpha, beta)
    if is_over(game_state)
        if winner(compute_game_result(game_state)) == game_state.next_player
            return (game_state.board.num_cols * game_state.board.num_rows, MovePass())
        else
            return (-(game_state.board.num_cols * game_state.board.num_rows), MovePass())
        end
    end

    if max_depth == 0
        return eval_fn(game_state)
    end

    best_result = -(game_state.board.num_cols * game_state.board.num_rows)
    best_move = MovePass()
    for candidate_move in legal_moves(game_state)
        next_state = apply_move(game_state, MovePlay(candidate_move))
        opp_best_result = alpha_beta_search(next_state, max_depth - 1, eval_fn, alpha, beta)[1]
        result = -opp_best_result
        if result > best_result
            best_result = result
            best_move = MovePlay(candidate_move)
        end
        if game_state.next_player == :white
            if best_result > beta
                beta = best_result
            end
            outcome_black = -best_result
            if outcome_black < alpha
                return (best_result, best_move)
            end
        else
            if best_result > alpha
                alpha = best_result
            end
            outcome_white = -best_result
            if outcome_white < beta
                return (best_result, best_move)
            end
        end
    end
    (best_result, best_move)
end


# module strategies
# using ..Helpers
# using ...GoTypes
# using ...goboard
# using ...scoring
# using FunctionalCollections, Test

# function random_move(game_state)
    # candidates = legal_moves(game_state)
    # if length(candidates) == 0
        # return MovePass()
    # else
        # return MovePlay(rand(candidates))
    # end
# end

# function minimax(game_state)
    # minimax_search(game_state, 2, capture_diff)[2]
# end
# end # module

end # module
