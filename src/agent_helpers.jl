module helpers
using FunctionalCollections
using Test

using ...goboard
using ...GoTypes

export is_eye, legal_moves, minimax_search, capture_diff, alpha_beta_search

function is_eye(board, point, color)
    if get_string(board,point) != nothing
        return false
    end

    if any(c -> c != color,
           map(p -> get_player(board, p),
               filter(n -> is_on_grid(board,n),
                      neighbors(point))))
        return false
    end

    corners = [(row=point.row - 1, col=point.col - 1),
               (row=point.row - 1, col=point.col + 1),
               (row=point.row + 1, col=point.col - 1),
               (row=point.row + 1, col=point.col + 1)]

    friendly_corners = reduce(+,
                              map(p -> get_player(board, p) == color ? 1 : 0,
                                  filter(c -> is_on_grid(board, c), corners)))
    
    off_board_corners = length(filter(c -> !is_on_grid(board, c), corners))
    
    if off_board_corners > 0
        return off_board_corners + friendly_corners == 4
    else
        return friendly_corners >= 3
    end
end

function legal_moves(game_state)
    candidates = Set{Point}()
    for r in 1:game_state.board.num_rows
        for c in 1:game_state.board.num_cols
            if is_valid_move(game_state, MovePlay(Point((row=r,col=c)))) &&
                !is_eye(game_state.board, Point((row=r,col=c)), game_state.next_player)
                push!(candidates, Point((row=r,col=c)))
            end
        end
    end
    candidates
end

function minimax_search(game_state, max_depth, eval_fn)
    if is_over(game_state)
        if winner(compute_game_result(game_state)) == game_state.next_player
            return (game_state.board.num_cols * game_state.board.num_rows, MovePass())
        else
            return (-(game_state.board.num_cols * game_state.board.num_rows), MovePass())
        end
    end

    if max_depth == 0
        return eval_fn(game_state)
    end

    best_result = -(game_state.board.num_cols * game_state.board.num_rows)
    best_move = MovePass()
    for candidate_move in legal_moves(game_state)
        next_state = apply_move(game_state, MovePlay(candidate_move))
        opp_best_result = minimax_search(next_state, max_depth - 1, eval_fn)[1]
        result = -opp_best_result
        if result > best_result
            best_result = result
            best_move = MovePlay(candidate_move)
        end
    end
    (best_result, best_move)
end



function capture_diff(game_state)
    black_stones = 0
    white_stones = 0
    for r in 1:game_state.board.num_rows
        for c in 1:game_state.board.num_cols
            if goboard.get_player(game_state.board, (row = r, col = c)) == :white
                white_stones += 1
            elseif goboard.get_player(game_state.board, (row = r, col = c)) == :black
                black_stones += 1
            end
        end
    end
    diff = black_stones - white_stones
    if game_state.next_player == :white
        return -diff
    else
        return diff
    end
end

@testset "position evaluation" begin
    board = goboard.GoBoard(5)
    board = goboard.place_stone(board, :black, (row=1,col=2))
    board = goboard.place_stone(board, :black, (row=1,col=4))
    board = goboard.place_stone(board, :black, (row=2,col=2))
    board = goboard.place_stone(board, :black, (row=2,col=3))
    board = goboard.place_stone(board, :black, (row=2,col=4))
    board = goboard.place_stone(board, :black, (row=2,col=5))
    board = goboard.place_stone(board, :black, (row=3,col=1))
    board = goboard.place_stone(board, :black, (row=3,col=2))
    board = goboard.place_stone(board, :black, (row=3,col=3))
    board = goboard.place_stone(board, :white, (row=3,col=4))
    board = goboard.place_stone(board, :white, (row=3,col=5))
    board = goboard.place_stone(board, :white, (row=4,col=1))
    board = goboard.place_stone(board, :white, (row=4,col=2))
    board = goboard.place_stone(board, :white, (row=4,col=3))
    board = goboard.place_stone(board, :white, (row=5,col=4))
    board = goboard.place_stone(board, :white, (row=5,col=5))
    @test capture_diff(goboard.GameState(:white, board, nothing, nothing, PersistentSet{Tuple{Symbol,Int64}}())) == -2
    @test capture_diff(goboard.GameState(:black, board, nothing, nothing, PersistentSet{Tuple{Symbol,Int64}}())) == 2
end

@testset "eye tests" begin
    state = new_game(5)
    state = apply_move(state, MovePlay((row=1,col=1)))
    @test is_eye(state.board, (row=1,col=1), :white) == false
    @test is_eye(state.board, (row=1,col=2), :white) == false
    state = new_game(5)
    state = apply_move(state, MovePlay((row=1, col=2))) # b
    state = apply_move(state, MovePlay((row=1, col=5))) # w
    state = apply_move(state, MovePlay((row=2, col=1))) # b
    state = apply_move(state, MovePlay((row=2, col=5))) # w
    state = apply_move(state, MovePlay((row=2, col=3))) # b
    state = apply_move(state, MovePlay((row=3, col=5))) # w
    state = apply_move(state, MovePlay((row=3, col=2))) # b
    @test is_eye(state.board, (row=2,col=2), :black) == false
    state = apply_move(state, MovePlay((row=4,col=5))) # w
    state = apply_move(state, MovePlay((row=1,col=1))) # b
    state = apply_move(state, MovePlay((row=3,col=4))) # w
    state = apply_move(state, MovePlay((row=1,col=3))) # b
    state = apply_move(state, MovePlay((row=3,col=3))) # w
    state = apply_move(state, MovePlay((row=3,col=1))) # b
    @test is_eye(state.board, (row=2,col=2), :black) == true
    state = new_game(5)
    state = apply_move(state, MovePlay((row=1,col=1))) # b
    state = apply_move(state, MovePlay((row=1,col=2))) # w
    state = apply_move(state, MovePlay((row=2,col=1))) # b
    state = apply_move(state, MovePlay((row=1,col=4))) # w
    state = apply_move(state, MovePlay((row=3,col=2))) # b
    state = apply_move(state, MovePlay((row=2,col=2))) # w
    state = apply_move(state, MovePlay((row=3,col=3))) # b
    state = apply_move(state, MovePlay((row=2,col=3))) # w
    state = apply_move(state, MovePlay((row=3,col=4))) # b
    state = apply_move(state, MovePlay((row=2,col=4))) # w
    state = apply_move(state, MovePlay((row=3,col=5))) # b
    state = apply_move(state, MovePlay((row=2,col=5))) # w
    @test is_eye(state.board, (row=1,col=3), :white) == true
    state = new_game(5)
    state = apply_move(state, MovePlay((row=1, col=2))) # b
    state = apply_move(state, MovePlay((row=1, col=5))) # w
    state = apply_move(state, MovePlay((row=2, col=1))) # b
    state = apply_move(state, MovePlay((row=2, col=5))) # w
    state = apply_move(state, MovePass()) # b
    state = apply_move(state, MovePlay((row=2, col=3))) # w
    state = apply_move(state, MovePlay((row=3, col=2))) # b
    state = apply_move(state, MovePlay((row=4,col=5))) # w
    state = apply_move(state, MovePlay((row=1,col=1))) # b
    state = apply_move(state, MovePlay((row=3,col=4))) # w
    state = apply_move(state, MovePlay((row=1,col=3))) # b
    state = apply_move(state, MovePlay((row=3,col=3))) # w
    state = apply_move(state, MovePlay((row=3,col=1))) # b
    @test is_eye(state.board, (row=2,col=2), :black) == false
end
end # module
