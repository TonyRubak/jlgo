module mcts
using ..goboard, ..GoTypes, ..agent, ..agent.helpers, ..scoring, Random
import ..agent.select_move
export MCTSAgent

mutable struct MCTSNode
    game_state::GameState
    parent
    move
    win_counts::Dict{Symbol,Int}
    num_rollouts::Int
    children::Array{MCTSNode}
    unvisited_moves::Array{Point}
end

function MCTSNode(state::GameState, parent = nothing, move = nothing)
    MCTSNode(state,
             parent,
             move,
             Dict(:black => 0, :white => 0),
             0,
             [],
             collect(legal_moves(state)))
end

function add_random_child!(node)
    moves = shuffle(node.unvisited_moves)
    new_move = moves[1]
    node.unvisited_moves = moves[2:end]
    new_state = apply_move(node.game_state, MovePlay(new_move))
    new_node = MCTSNode(new_state, node, new_move)
    push!(node.children, new_node)
    new_node
end

function record_win!(node, winner)
    node.win_counts[winner] += 1
    node.num_rollouts += 1
end

function can_add_child(node)
    length(node.unvisited_moves) > 0
end

function is_terminal(node)
    is_over(node.game_state)
end

function winning_fraction(node, player)
    node.win_counts[player] / node.num_rollouts
end

struct MCTSAgent <: Agent
    num_rounds::Int
    temperature::Float64
end

function select_move(agent::MCTSAgent, state)
    if winner(compute_game_result(state)) == state.next_player && state.last_move == MovePass()
        return MovePass()
    end

    root = MCTSNode(state)

    for i in 1:agent.num_rounds
        node = root
        while !(node === nothing) && !can_add_child(node) && !is_terminal(node)
            node = select_child(agent, node)
        end

        if !(node === nothing) && can_add_child(node)
            node = add_random_child!(node)
        end

        winner = simulate_random_game(state)

        while !(node === nothing)
            record_win!(node, winner)
            node = node.parent
        end
    end

    best_move = nothing
    best_pct = -1.0
    for child in root.children
        child_pct = winning_fraction(child, state.next_player)
        if child_pct > best_pct
            best_pct = child_pct
            best_move = child.move
        end
    end

    if best_pct <= 0.1
        MoveResign()
    else
        MovePlay(best_move)
    end
end

function simulate_random_game(state)
    bots = Dict(:black => FastRandomAgent(),
                :white => FastRandomAgent())

    while !is_over(state)
        move = select_move(bots[state.next_player], state)
        state = apply_move(state, move)
    end

    winner(compute_game_result(state))
end

function uct_score(parent_rollouts, child_rollouts, win_pct, temperature)
    exploration = sqrt(log(parent_rollouts / child_rollouts))
    win_pct + temperature * exploration
end

function select_child(agent, node)
    total_rollouts = reduce(+, child.num_rollouts for child in node.children; init = 0)

    best_score = -1
    best_child = nothing
    for child in node.children
        score = uct_score(total_rollouts, child.num_rollouts,
                          winning_fraction(child, node.game_state.next_player),
                          agent.temperature)
        if score > best_score
            best_score = score
            best_child = child
        end
    end
    best_child
end
end
