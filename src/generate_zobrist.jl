function to_julia(state)
    if state == nothing
        "nothing"
    else
        ":" * String(state)
    end
end

MAX63 = 0x7fffffffffffffff

table = Dict()
empty_board = 0

for row in 1:20
    for col in 1:20
        for state in [:black, :white]
            table[(row=row,col=col), state] = rand(0:MAX63)
        end
    end
end

println("module Zobrist")
println("using ..GoTypes")
println("export HASH_CODE, EMPTY_BOARD")
println("const HASH_CODE = Dict{Tuple{Point,Symbol},Int64}(")
for ((pt,state), hash_code) in pairs(table)
    println("\t($pt, $(to_julia(state))) => $hash_code,")
end
println(")")
println("const EMPTY_BOARD = $empty_board")
println("end # module")
