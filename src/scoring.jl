module scoring
using Test
using ..goboard, ..GoTypes

export Territory, compute_game_result, winner, margin
import Base.print

struct Territory
    black_territory
    white_territory
    black_stones
    white_stones
    dame
    dame_points
end

function Territory(territory_map)
    black_territory = 0
    white_territory = 0
    black_stones = 0
    white_stones = 0
    dame = 0
    dame_points = []

    for (point, status) in pairs(territory_map)
        if status == :black
            black_stones += 1
        elseif status == :white
            white_stones += 1
        elseif status == "territory_b"
            black_territory += 1
        elseif status == "territory_w"
            white_territory += 1
        elseif status == "dame"
            dame += 1
            push!(dame_points, point)
        else
            @assert false
        end
    end
    Territory(black_territory,white_territory,black_stones,white_stones,dame,dame_points)
end

function Territory(board::goboard.GoBoard)
    status = Dict()
    for p in [(row=r,col=c) for (r,c) in Iterators.product(1:board.num_rows,1:board.num_cols)]
        if p in keys(status)
            continue
        elseif get_player(board, p) != nothing
            status[p] = get_player(board, p)
        else
            group, neighbors = collect_region(p, board)
            fill_with = length(neighbors) == 1 ? "territory_" * (pop!(neighbors) == :black ? "b" : "w") : "dame"
            setindex!(status, fill_with, group...)
        end
    end
    Territory(status)
end

function collect_region(start_pos, board, visited=nothing)
    if visited == nothing
        visited = Set()
    elseif start_pos in visited
        return ([], Set())
    end

    all_points = [start_pos]
    all_borders = Set()
    push!(visited,start_pos)
    here = get_player(board, start_pos)
    deltas = [(-1,0),(1,0),(0,-1),(0,1)]
    for (delta_r, delta_c) in deltas
        next_p = (row=start_pos.row + delta_r,
                  col=start_pos.col + delta_c)
        if !is_on_grid(board,next_p)
            continue
        end
        neighbor = get_player(board, next_p)
        if neighbor == here
            points, borders = collect_region(next_p, board, visited)
            all_points = vcat(all_points, points)
            all_borders = union(all_borders, borders)
        else
            push!(all_borders, neighbor)
        end
    end
    (all_points, all_borders)
end

const GameResult = NamedTuple{(:b,:w,:komi),Tuple{Int,Int,Float64}}

function compute_game_result(game)
    territory = Territory(game.board)
    (b = territory.black_territory + territory.black_stones,
     w = territory.white_territory + territory.white_stones,
     komi = 7.5)
end

function winner(result)
    if result.b > result.w + result.komi
        return :black
    else
        return :white
    end
end

margin(result) = abs(result.b - (result.w + result.komi))

function print(io::IO, result::GameResult)
    print(io, (winner(result) == :black ? "B+" : "W+") * string(margin(result)))
end

@testset "scoring test" begin
    # .w.ww
    # wwww.
    # bbbww
    # .bbbb
    # .b.b.
    board = goboard.GoBoard(5)
    board = goboard.place_stone(board, :black, (row=1,col=2))
    board = goboard.place_stone(board, :black, (row=1,col=4))
    board = goboard.place_stone(board, :black, (row=2,col=2))
    board = goboard.place_stone(board, :black, (row=2,col=3))
    board = goboard.place_stone(board, :black, (row=2,col=4))
    board = goboard.place_stone(board, :black, (row=2,col=5))
    board = goboard.place_stone(board, :black, (row=3,col=1))
    board = goboard.place_stone(board, :black, (row=3,col=2))
    board = goboard.place_stone(board, :black, (row=3,col=3))
    board = goboard.place_stone(board, :white, (row=3,col=4))
    board = goboard.place_stone(board, :white, (row=3,col=5))
    board = goboard.place_stone(board, :white, (row=4,col=1))
    board = goboard.place_stone(board, :white, (row=4,col=2))
    board = goboard.place_stone(board, :white, (row=4,col=3))
    board = goboard.place_stone(board, :white, (row=4,col=4))
    board = goboard.place_stone(board, :white, (row=5,col=2))
    board = goboard.place_stone(board, :white, (row=5,col=4))
    board = goboard.place_stone(board, :white, (row=5,col=5))
    territory = Territory(board)
    @test territory.black_stones == 9
    @test territory.black_territory == 4
    @test territory.white_stones == 9
    @test territory.white_territory == 3
    @test territory.dame == 0
end
end # module
