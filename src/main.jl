module Main

using Test
using ..goboard, ..GoTypes
using ..scoring
using ..agent, ..mcts

function main(board_size)
    game = new_game(board_size)
    bots = Dict(:black => AlphaBetaAgent(2, agent.helpers.capture_diff),
                :white => MCTSAgent(100,1.5))

    while !is_over(game)
        show_board(game.board)
        move = select_move(bots[game.next_player], game)
        game = apply_move(game, move)
    end

    result = compute_game_result(game)
    println("Player $(winner(result)) wins!")
    println("Score was $result.")
end

function vs_player(board_size)
    game = new_game(board_size)
    bot = Agent(Agents.strategies.random_move)

    while !is_over(game)
        show_board(game.board)
        move = if game.next_player == :black
            print("-- ")
            MovePlay(string_to_coords(chomp(readline())))
        else
            bot.select_move(game)
        end
        game = apply_move(game, move)
    end

    result = compute_game_result(game)
    println("Player $(winner(result)) wins!")
    println("Score was $result.")
end

function string_to_coords(coord_string)
    colnames = ['A', 'B', 'C', 'D', 'E', 'F', 'G',
                'H', 'I', 'J', 'K', 'L', 'M', 'N',
                'O', 'P', 'Q', 'R', 'S', 'T', 'U']
    col_n = indexin(coord_string[1], colnames)[1]
    row_n = parse(Int, coord_string[2:length(coord_string)])
    (row = row_n, col = col_n)
end

@testset "string to coords" begin
    @test string_to_coords("A1") == (row = 1, col = 1)
    @test string_to_coords("B3") == (row = 3, col = 2)
end
end # module
