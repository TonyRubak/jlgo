module goboard
using FunctionalCollections, Test
using ..Zobrist
using ..GoTypes

import Base.==

export GameState
export new_game
export Move, MovePlay, MovePass, MoveResign
export is_on_grid, get_player, get_string
export apply_move, is_valid_move, is_over
export show_board
# Move
abstract type Move end
struct MovePlay <: Move
    point::Point
end

struct MovePass <: Move
end

struct MoveResign <: Move
end

struct MoveNothing <: Move end

# GoString

struct GoString
    color::Symbol
    liberties::PersistentSet{Point}
    stones::PersistentSet{Point}
end

function ==(s1::GoString,s2::GoString)
    s1.color == s2.color &&
        s1.liberties == s2.liberties &&
        s1.stones == s2.stones
end

make_gostring(color) = GoString(color,
                                PersistentSet{Point}(),
                                PersistentSet{Point}())

make_gostring(color,stones,liberties) = GoString(color,liberties,stones)

                                                          

add_liberty(string, point) = GoString(string.color,
                                      conj(string.liberties, point),
                                      string.stones)
                                           

remove_liberty(string, point) = GoString(string.color,
                                         disj(string.liberties,point),
                                         string.stones)

num_liberties(string) = length(string.liberties)

function merged_with(string,other_string)
    @assert string.color == other_string.color
    combined_stones = union(string.stones,other_string.stones)
    make_gostring(string.color,combined_stones,
                  setdiff(union(string.liberties,other_string.liberties),combined_stones))
end

# GoBoard

struct GoBoard
    num_rows::Int64
    num_cols::Int64
    grid::PersistentHashMap{Point,GoString}
    hash::Int64
end

GoBoard(size) = GoBoard(size,
                        size,
                        PersistentHashMap{Point,GoString}(),
                        Zobrist.EMPTY_BOARD)

GoBoard(board, grid, hash) = GoBoard(board.num_rows, board.num_cols, grid, hash)

is_on_grid(board,point) = (1 <= point.row <= board.num_rows) &&
    (1 <= point.col <= board.num_cols)

function get_player(board,point)
    if !haskey(board.grid,point)
        return nothing
    else
        return board.grid[point].color
    end
end

function get_string(board,point)
    if !haskey(board.grid,point)
        return nothing
    else
        return board.grid[point]
    end
end

function place_stone(board, player, point)
    function remove_string(string, grid, hash)
        for point in string.stones
            for neighbor in neighbors(point)
                neighbor_string = if !haskey(grid,neighbor)
                    nothing
                else
                    grid[neighbor]
                end
                if neighbor_string == nothing
                    continue
                elseif neighbor_string != string
                    neighbor_string = add_liberty(neighbor_string, point)
                    grid = reduce((g,p) -> assoc(g,p,neighbor_string),
                                  neighbor_string.stones;
                                  init=grid)
                end
            end
            grid = dissoc(grid, point)
            hash = xor(hash, Zobrist.HASH_CODE[point, string.color])
        end
        (grid, hash)
    end

    function get_adjacent(player)
        PersistentSet{GoString}(filter(x -> x != nothing && x.color == player,
                             [get_string(board,n) for n in neighbors(point)]))
    end

    @assert is_on_grid(board,point)
    @assert get_string(board,point) == nothing

    liberties = PersistentSet(filter(x -> is_on_grid(board,x) && get_string(board,x) == nothing,
                       [n for n in neighbors(point)]))

    new_string = reduce(merged_with,
                        get_adjacent(player);
                        init=make_gostring(player, conj(PersistentSet{Point}(),point), liberties))

    hash = xor(board.hash, Zobrist.HASH_CODE[point, player])

    grid= reduce((g, p) -> assoc(g, p, new_string),
                  new_string.stones;
                  init=board.grid)

    for s::GoString in get_adjacent(other(player))
        if length(disj(s.liberties,point)) > 0
            new_opp_string = make_gostring(other(player), s.stones, disj(s.liberties,point))
            grid = reduce((g,p) -> assoc(g,p,new_opp_string), s.stones; init=grid)
        end
    end
    
    for s::GoString in get_adjacent(other(player))
        if length(disj(s.liberties,point)) == 0
            (grid,hash) = remove_string(s,grid,hash)
        end
    end
    GoBoard(board, grid, hash)
end

function show_board(board)
    for i in board.num_rows:-1:1
        for j in 1:board.num_cols
            if get_player(board,(row=i,col=j)) == nothing
                print("_")
            elseif get_player(board,(row=i,col=j)) == :black
                print("b")
            else
                print("w")
            end
        end
        println()
    end
    println()
end

# GameState

struct GameState
    next_player::Symbol
    board::GoBoard
    last_move::Union{Nothing,Move}
    previous_state::Union{Nothing,GameState}
    previous_states::PersistentSet{Tuple{Symbol,Int64}}
end

function GameState(board, next_player, previous, move)
    previous_states = if previous == nothing
        PersistentSet{Tuple{Symbol,Int64}}()
    else
        conj(previous.previous_states,(previous.next_player,previous.board.hash))
    end
    GameState(next_player,
              board,
              move,
              previous,
              previous_states)
end

make_gamestate(board,next_player,previous,move) = GameState(board,next_player,previous,move)

new_game(size) = GameState(GoBoard(size), :black, nothing, nothing)

function apply_move(state, move::MovePlay)
    make_gamestate(place_stone(state.board, state.next_player, move.point),
                   other(state.next_player),
                   state,
                   move)
end

function apply_move(state, move::Move)
    make_gamestate(state.board,
                   other(state.next_player),
                   state,
                   move)
end

function is_over(state)
    _is_over(::Nothing, ::Any) = false
    _is_over(::MoveResign, ::Move) = true
    _is_over(::MoveResign, ::Nothing) = true
    _is_over(::MovePlay, ::Nothing) = false
    _is_over(::MovePass, ::Nothing) = false
    _is_over(::MovePass, ::MovePass) = true
    _is_over(::Move, ::Move) = false
    second_last_move = if state.previous_state == nothing
        nothing
    else
        state.previous_state.last_move
    end
    _is_over(state.last_move, second_last_move)
end

is_move_self_capture(board, player, move::Move) = false

function is_move_self_capture(board::GoBoard, player, move::MovePlay)
    num_liberties(get_string(board, move.point)) == 0
end

is_move_ko(board, player, move::Move) = false

function is_move_ko(board::GoBoard, state::GameState, player)
    (other(player), board.hash) in state.previous_states
end

function is_valid_move(state, move)
    _is_valid_move(::Move) = true
    function _is_valid_move(move::MovePlay)
        if get_string(state.board, move.point) == nothing
            next_board = place_stone(state.board, state.next_player, move.point)
            return !is_move_self_capture(next_board, state.next_player, move) &&
                !is_move_ko(next_board, state, state.next_player)
        else
            return false
        end
    end
    if is_over(state)
        return false
    else
        _is_valid_move(move)
    end
end

@testset "GoString tests" begin
    s = make_gostring(:black)
    @test add_liberty(s,(row=1,col=1)).liberties == push(PersistentSet(),(row=1,col=1))
    s = add_liberty(s, (row=1,col=1))
    @test add_liberty(s,(row=2,col=2)).liberties ==
        push(push(PersistentSet(),(row=1,col=1)),(row=2,col=2))
    s = add_liberty(s, (row=2,col=2))
    @test remove_liberty(s,(row=1,col=1)).liberties ==
        push(PersistentSet(),(row=2,col=2))
    @test remove_liberty(s,(row=2,col=2)).liberties ==
        push(PersistentSet(),(row=1,col=1))
    t = add_liberty(make_gostring(:black),(row=3,col=4))
    u = add_liberty(make_gostring(:white),(row=3,col=4))
    v = add_liberty(make_gostring(:white),(row=3,col=4))
    @test_throws AssertionError merged_with(s,u)
    @test merged_with(s,t).liberties ==
        push(push(push(PersistentSet(),(row=1,col=1)),(row=2,col=2)),(row=3,col=4))
    @test s != t
    @test u == v
end

@testset "GoBoard tests" begin
    b = GoBoard(19)
    @test is_on_grid(b,(row=0,col=1)) == false
    @test is_on_grid(b,(row=1,col=1)) == true
    @test is_on_grid(b,(row=1,col=0)) == false
    @test is_on_grid(b,(row=1,col=20)) == false
    @test is_on_grid(b,(row=20,col=1)) == false
    @test_throws AssertionError place_stone(b,:black,(row=-1,col=10))
    @test get_player(b,(row=1,col=1)) == nothing
    @test get_string(b,(row=1,col=1)) == nothing
    @test place_stone(b,:white,(row=1,col=1)).grid[(row=1,col=1)].color == :white
    b2 = place_stone(b,:white,(row=1,col=1))
    b2 = place_stone(b2,:black,(row=1,col=2))
    b2 = place_stone(b2,:black,(row=2,col=1))
    @test haskey(b2.grid,(row=1,col=1)) == false
    @test in((row=1,col=1), get_string(b2,(row=2,col=1)).liberties) == true
    b2 = place_stone(b,:white,(row=1,col=1))
    b2 = place_stone(b2,:white,(row=2,col=1))
    @test b2.grid[(row=1,col=1)] == b2.grid[(row=2,col=1)]
end
@testset "GoState tests" begin
    @test new_game(19).next_player == :black
    state = new_game(19)
    @test apply_move(state, MovePlay((row=1,col=1))).next_player == :white
    @test apply_move(state, MovePass()).next_player == :white
    @test is_over(apply_move(state,MoveResign())) == true
    @test is_over(apply_move(state, MovePlay((row=1,col=1)))) == false
    @test is_over(state) == false
    @test is_over(apply_move(apply_move(state,MovePass()), MovePass())) == true
    @test is_over(apply_move(apply_move(state,MovePass()), MoveResign())) == true
    @test is_move_self_capture(state, :white, MovePass()) == false
    next_state = apply_move(state, MovePlay((row=1,col=1))) # b
    next_state = apply_move(next_state, MovePlay((row=2,col=1))) # w
    next_state = apply_move(next_state, MovePlay((row=19,col=1))) # b
    next_state = apply_move(next_state, MovePlay((row=2, col=2))) # w
    next_state = apply_move(next_state, MovePlay((row=19,col=2))) # b
    next_state_sc = apply_move(next_state, MovePlay((row=1, col=3))) # w
    next_state = apply_move(state, MovePlay((row=1,col=2))) # b
    next_state = apply_move(next_state, MovePlay((row=2,col=2))) # w
    next_state = apply_move(next_state, MovePlay((row=2,col=1))) # b
    next_state = apply_move(next_state, MovePlay((row=3,col=1))) # w
    next_state = apply_move(next_state, MovePlay((row=2,col=3))) # b
    next_state = apply_move(next_state, MovePlay((row=3,col=3))) # w
    next_state = apply_move(next_state, MovePass()) # b
    next_state = apply_move(next_state, MovePlay((row=4,col=2))) # w
    next_state_ko = apply_move(next_state, MovePlay((row=3,col=2))) # b
    @test is_valid_move(apply_move(state, MoveResign()), MovePlay((row=1,col=1))) == false
    @test is_valid_move(state, MovePass()) == true
    @test is_valid_move(apply_move(state, MovePlay((row=1,col=1))), MovePlay((row=1,col=1))) == false
    @test is_valid_move(next_state_sc, MovePlay((row=1,col=2))) == false
    @test is_valid_move(state, MovePlay((row=1,col=1))) == true
    @test is_valid_move(next_state_ko, MovePlay((row=2,col=2))) == false
end

end # module
